import threading
import socket
import json
import time
from control import menu, cleanScreen, printMenu
#import view

clients = []
addresses = []
username = "Central"

def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        print("\nWaiting to connect\n")
        server.bind(('localhost', 10851))
        server.listen()
    except:
        return print('\nNão foi possível iniciar o servidor!\n')

    while True:
        client, addr = server.accept()
        clients.append(client)
        addresses.append(addr)

        threadReceiv = threading.Thread(target=receiveMessages, args=[client, addr])
        threadReceiv.start()

        menu(client)

            
def broadcast(msg):
    for i in range(0, len(clients)):
            try:
                clients[i].send(msg)
            except:
                deleteClient(clients[i],addresses[i])

def deleteClient(client, addr):
    clients.remove(client)
    addresses.remove(addr)


def receiveMessages(client, addr):
    while True:
        #printMenu()
        time.sleep(3)
        try:
            msg = client.recv(2048)
            if len(msg) == 0:
                return
            else:
                print(msg.decode('utf-8')+'\n')                            
        except:
            deleteClient(client, addr)
            return       
        
        #menu(client)  

main()