import csv
import time

def menu(client):
    comCSV = []
    with open('data/userInputs.csv', 'a', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        ans = -1
        while ans != 0:
            printMenu()
            ans = int(input('Digite sua escolha: '))
            sendCommand(ans,client, comCSV)
            writeCSV(writer,comCSV)
                
def printMenu():
    print("## MENU DE COMANDO ## \n \
    Digite 0 para sair\n \
    Digite 1 para mudar o estado da lampada 2\n \
    Digite 2 para mudar o estado da lampada 1\n \
    Digite 3 para mudar o estado do Ar condicionado\n \
    Digite 4 para mudar o estado do Projetor\n \
    Digite 5 para mudar o estado do  Alarme\n \
    Digite 6 para ligar tudo\n \
    Digite 7 para desligar tudo\n")

def sendCommand(ans, client, comCSV):
    if ans == 0:
        return
    elif ans == 1:
        msg = "Mudando estado da lampada 2..."
        print(msg)
        client.send(f'{ans}'.encode('utf-8'))
    elif ans == 2:
        msg = "Mudando estado da lampada 1..."
        print(msg)        
        client.send(f'{ans}'.encode('utf-8'))
    elif ans == 3:
        msg = "Mudando estado do ar condicionado..."
        print(msg)            
        client.send(f'{ans}'.encode('utf-8'))
    elif ans == 4:
        msg = "Mudando estado do projetor..."
        print(msg)
        client.send(f'{ans}'.encode('utf-8'))
    elif ans == 5:
        msg = "Mudando estado do alarme..."
        print(msg)
        client.send(f'{ans}'.encode('utf-8'))
    elif ans == 6:
        msg = "Ligando tudo..."
        print(msg)
        client.send(f'{ans}'.encode('utf-8'))
    elif ans == 7:
        msg = "Desligando tudo..."        
        print(msg)
        client.send(f'{ans}'.encode('utf-8'))        
    else:
        pass
    
    comCSV.append(msg)


def writeCSV(writer,comCSV):
    for command in comCSV:
        writer.writerow([command])
    comCSV.clear()


def cleanScreen():
    counter = 20
    while counter > 0:
        print("\n")
        counter-=1
