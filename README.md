# Trabalho 1 da disciplina de Fundamentos de Sistemas Embarcados (2022/2)

Nome: Rodrigo Balbino Azevedo de Brito \
Matrícula: 190048221

# Download

Clone o repositório em uma pasta de seu computador.

Copie a pasta 'Distribuidos' para dentro da raspberry por meio do ssh e tunnelamento.

## Executando o programa

Inicialmente, execute o arquivo main.py localizado dentro da pasta 'Central' em sua máquina.

Em seguida, execute o arquivo main.py localizado na pasta 'Distribuidos' que foi colocado dentro do ssh da raspberry.

## Utilização

Para utilizar o programa, siga as instruções dos comandos abaixo:

    Digite 0 para sair\n \
    Digite 1 para mudar o estado da lampada 2 \
    Digite 2 para mudar o estado da lampada 1 \
    Digite 3 para mudar o estado do Ar condicionado \
    Digite 4 para mudar o estado do Projetor\n \
    Digite 5 para mudar o estado do  Alarme\n \
    Digite 6 para ligar tudo\n \
    Digite 7 para desligar tudo\n")

Também é possível utilizar as dashboards disponibilizadas pelo professor para alterar os sensores.
