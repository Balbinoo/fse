import RPi.GPIO as GPIO
from setup import *
from view import print_alert

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

def changeState_lamp2(hostName):
    lamp2 = setLamp2Pin(hostName)    
    GPIO.setup(lamp2, GPIO.OUT)    
    state = GPIO.input(lamp2)

    if state:
        GPIO.output(lamp2, GPIO.LOW)
        msg = "A lampada 2 agora está apagada"
        print(msg)
        return print_alert(msg)
    else:
        GPIO.output(lamp2, GPIO.HIGH)
        msg = "A lampada 2 agora está acesa"
        print(msg)
        return print_alert(msg)
        


def changeState_lamp1(hostName):
    lamp1 = setLamp1Pin(hostName)
    GPIO.setup(lamp1, GPIO.OUT)
    state = GPIO.input(lamp1)
    
    if state:
        GPIO.output(lamp1, GPIO.LOW)
        msg = "A lampada 1 agora está apagada"
        return print_alert(msg)
    else:
        GPIO.output(lamp1, GPIO.HIGH)
        msg = "A lampada 1 agora está acesa"
        return print_alert(msg)

def changeState_ar(hostName):
    ar = setArPin(hostName)
    GPIO.setup(ar, GPIO.OUT)
    state = GPIO.input(ar)
    
    if state:
        GPIO.output(ar, GPIO.LOW)
        msg = "O ar condicionado agora está desligado"
        return print_alert(msg)
    else:
        GPIO.output(ar, GPIO.HIGH)
        msg = "O ar condicionado agora está ligado"
        return print_alert(msg)
        

def changeState_Projetor(hostName):
    projetor = setProjetorPin(hostName)
    GPIO.setup(projetor, GPIO.OUT)
    state = GPIO.input(projetor)
    
    if state:
        GPIO.output(projetor, GPIO.LOW)
        msg = "O projetor agora está desligado"
        return print_alert(msg)
    else:
        GPIO.output(projetor, GPIO.HIGH)
        msg = "O projetor agora está ligado"
        return print_alert(msg)



def changeState_Alarme(hostName):
    alarme = setAlarmePin(hostName)
    state = check_AlarmState(hostName)
    sensor = []
    if state == 0:
        sensor = checkSensors(hostName)
        if sensor == []:
            GPIO.output(alarme, GPIO.HIGH)
            msg = "O alarme está ligado"
            return print_alert(msg)
        else:
            msg = f"Desative todos os sensores: {sensor} antes de ligar o alarme"
            return print_alert(msg)
    else:
        GPIO.output(alarme, GPIO.LOW)
        msg = "O alarme está desligado"
        return print_alert(msg)


def checkSensors(hostName):
    print("Entrou no check sensors")
    sensors = []
    if check_FumacaState(hostName):
        print("EEntrou em fumaca?\n")
        sensors.append("Fumaça")
    if check_JanelaState(hostName):
        sensors.append("Janela")
    if check_PortaState(hostName):
        sensors.append("Porta")
    if check_Presenca(hostName):
        sensors.append("Presença")
    print("Sensores!")
    print(sensors)
    return sensors


def turnAll_high(hostName):
    lamp2, lamp1, ar, projetor, alarme = validateOutputPins(hostName)

    setupOutputs(lamp2, lamp1, ar, projetor, alarme) 
    
    GPIO.output(lamp2, GPIO.HIGH)
    GPIO.output(lamp1, GPIO.HIGH)
    GPIO.output(ar, GPIO.HIGH)
    GPIO.output(projetor, GPIO.HIGH)
    GPIO.output(alarme, GPIO.HIGH)

    msg = "Todos os dispositivos foram ligados "
    return print_alert(msg)


def turnAll_down(hostName):
    lamp2, lamp1, ar, projetor, alarme = validateOutputPins(hostName)
    setupOutputs(lamp2, lamp1, ar, projetor, alarme) 
    
    GPIO.output(lamp2, GPIO.LOW)
    GPIO.output(lamp1, GPIO.LOW)
    GPIO.output(ar, GPIO.LOW)
    GPIO.output(projetor, GPIO.LOW)
    GPIO.output(alarme, GPIO.LOW)

    msg = "Todos os dispositivos foram desligados "
    return print_alert(msg)

def fire_Alarm(hostName):
    alarme = setAlarmePin(hostName)

    GPIO.setup(alarme, GPIO.OUT)
    GPIO.output(alarme, GPIO.HIGH)

def startLamps_15s(hostName):
    lp = setLamp1Pin(hostName)
    lp2 = setLamp2Pin(hostName)

    GPIO.setup(lp, GPIO.OUT)
    GPIO.setup(lp2, GPIO.OUT)
 
    GPIO.output(lp2, GPIO.HIGH)
    GPIO.output(lp, GPIO.HIGH)


    