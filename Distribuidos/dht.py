import time
import board
import adafruit_dht
from control import validateDhtPin

def dht22(dhtDevice):  
    try:
        temperature_c = dhtDevice.temperature
        temperature_f = temperature_c * (9 / 5) + 32
        humidity = dhtDevice.humidity
        return "Temp: {:.1f} F / {:.1f} C    Humidity: {}% ".format(temperature_f, temperature_c, humidity)            
        
    except RuntimeError as error:
        return error.args[0]
    except Exception as error:
        dhtDevice.exit()
        return "Exception error"
