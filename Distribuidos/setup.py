import RPi.GPIO as GPIO
import time
import board
import adafruit_dht

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

def setInputPin24():
    return 9,0,11,10,22,27 

def setInputPin13():
    return 12, 7, 1, 16, 20, 21    

def setOutputPin24():
    return  19, 26, 13, 6, 5    

def setOutputPin13():
    return 23, 18, 24, 25,8     

def setupOutputs(lamp2, lamp1, ar, projetor, alarme):
    GPIO.setup(lamp2, GPIO.OUT) 
    GPIO.setup(lamp1, GPIO.OUT)
    GPIO.setup(ar, GPIO.OUT)
    GPIO.setup(projetor, GPIO.OUT)
    GPIO.setup(alarme, GPIO.OUT)

def setupInputs(janela,presenca,fumaca,porta,pessoasIn,pessoasOut):
    GPIO.setup(janela, GPIO.IN)
    GPIO.setup(presenca, GPIO.IN)
    GPIO.setup(fumaca, GPIO.IN)
    GPIO.setup(porta, GPIO.IN)
    GPIO.setup(pessoasIn, GPIO.IN)
    GPIO.setup(pessoasOut, GPIO.IN)   

def setupDoorIn_Out(door_in,door_out):
    GPIO.setup(door_in, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(door_out, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def stateInputs(janela,presenca,fumaca,porta,pessoasIn,pessoasOut):
    return GPIO.input(janela), GPIO.input(presenca), GPIO.input(fumaca), GPIO.input(porta), GPIO.input(pessoasIn), GPIO.input(pessoasOut)    

def stateOutputs(lamp2, lamp1, ar, projetor, alarme):
    return GPIO.input(lamp2), GPIO.input(lamp1), GPIO.input(ar), GPIO.input(projetor), GPIO.input(alarme)
    
def validateHost(hostName):
    if hostName == "rasp44" or hostName == "rasp43":#SALA 2 E 4
        return 1
    else: #SALA 1 E 3
        return 0

def validateOutputPins(hostName):
    return setOutputPin24() if validateHost(hostName) else  setOutputPin13()

def validateInputPins(hostName):
    return setInputPin24() if validateHost(hostName) else setInputPin13()
            
def validateDhtPin(hostName):
    return adafruit_dht.DHT22(board.D18) if hostName == "rasp44" or hostName else adafruit_dht.DHT22(board.D4)   

#output pin

def setLamp2Pin(hostName):
    return 19 if validateHost(hostName) else 23

def setLamp1Pin(hostName):
    return 26 if validateHost(hostName) else 18

def setArPin(hostName):
    return 13 if validateHost(hostName) else 24

def setProjetorPin(hostName):
    return 6 if validateHost(hostName) else 25

def setdoorIn_Pin(hostName):
    return 22 if validateHost(hostName) else 20

def setdoorOut_Pin(hostName):
    return 27 if validateHost(hostName) else 21


#inputs pin
def setAlarmePin(hostName):
    return 5 if validateHost(hostName) else 8

def setFumacaPin(hostName):
    return 11 if validateHost(hostName) else 1

def setJanelaPin(hostName):    
    return 9 if validateHost(hostName) else 12

def setPortaPin(hostName):
    return 10 if validateHost(hostName) else 16

def setPresencaPin(hostName):
    return 0 if validateHost(hostName) else 7

          
# check State

def check_AlarmState(hostName):
    alarme = setAlarmePin(hostName)
    GPIO.setup(alarme, GPIO.OUT)
    return GPIO.input(alarme)

def check_FumacaState(hostName):
    fumaca = setFumacaPin(hostName)
    GPIO.setup(fumaca, GPIO.IN)
    return GPIO.input(fumaca)

def check_JanelaState(hostName):
    janela = setJanelaPin(hostName)
    GPIO.setup(janela, GPIO.IN)
    return GPIO.input(janela)

def check_PortaState(hostName):
    porta = setPortaPin(hostName)
    GPIO.setup(porta, GPIO.IN)
    return GPIO.input(porta)
    
def check_Presenca(hostName):
    presenca = setPresencaPin(hostName)
    GPIO.setup(presenca, GPIO.IN)
    return GPIO.input(presenca)


#
def check_Lamp2State(hostName):
    lamp2 = setLamp2Pin(hostName)
    GPIO.setup(lamp2, GPIO.IN)
    return GPIO.input(lamp2)
