import threading
import socket
import time
from control import *
from monitoring import listenInputs,listenOutputs,setupDoorIn_Out
from dht import dht22

#rasp42 - 01, rasp46 - 03
#rasp44 - 02, rasp43 - 04

def main():
    port = 10851
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Conectando com Central')

    try:
        client.connect(('localhost', port))
    except:  
        print(' Nao foi possivel se conectar a central')     
        return 
    
    print("\nConectado com sucesso!\n")

    hostName = socket.gethostname()
    print(f"\nHostName: {hostName}\n")
    client.send(f'\n{hostName}\n'.encode('utf-8'))

    recMessages_thread = threading.Thread(target=receiveMessages, args=[client, hostName])
    listen_thread = threading.Thread(target=listen_inputs_outputs, args=[client, hostName])
    dht22_thread = threading.Thread(target=listen_DHT22, args=[client, hostName])
    countPeople = threading.Thread(target=detectDoorIn_Out, args=[client, hostName])

    recMessages_thread.start()
    dht22_thread.start()
    listen_thread.start()
    countPeople.start()


def receiveMessages(client, hostName):

    while True:
        try: 
            msg = client.recv(2048).decode('utf-8')
            print(msg+'\n')
            if msg == '1':
                confirm = changeState_lamp2(hostName)
                client.send(f'\n{confirm}'.encode('utf-8'))
            elif msg == '2':
                confirm = changeState_lamp1(hostName)
                client.send(f'\n{confirm}'.encode('utf-8'))
            elif msg == '3':
                confirm = changeState_ar(hostName)
                client.send(f'\n{confirm}'.encode('utf-8'))
            elif msg == '4':
                confirm = changeState_Projetor(hostName)
                client.send(f'\n{confirm}'.encode('utf-8'))
            elif msg == '5':
              confirm = changeState_Alarme(hostName)
              client.send(f'\n{confirm}'.encode('utf-8'))
            elif msg == '6':
                confirm = turnAll_high(hostName)
                client.send(f'\n{confirm}'.encode('utf-8'))
            elif msg == '7':
                confirm = turnAll_down(hostName)
                client.send(f'\n{confirm}'.encode('utf-8'))
            else:
                pass
        except:
            print('\nNão foi possível permanecer conectado no servidor!\n')
            print('Pressione <Enter> para continuar...')
            client.close()
            break


def listen_inputs_outputs(client, hostName):

    while True:
        try:
            
            msgOuput = listenOutputs(hostName)
            msgInput, alarmFire, alarmRoom = listenInputs(hostName)

            client.send(f'----------------- \n \
                   \n{msgOuput}'.encode('utf-8'))
            client.send(f' \n{msgInput} \n \
                        -----------------'.encode('utf-8'))
            
            if(alarmFire != "0"):
                client.send(f'\n{alarmFire}\n'.encode('utf-8'))
            if(alarmRoom != "0"):
                client.send(f'\n{alarmRoom}\n'.encode('utf-8'))
            
            time.sleep(3)
        except:
            return

def listen_DHT22(client, hostName):
    dhtDevice = validateDhtPin(hostName)
    print("Entrou aqui no dht?")
    while True:
        msgDht = dht22(dhtDevice)

        client.send(f'\n{msgDht}'.encode('utf-8'))
        time.sleep(3)
        if msgDht == "Exception error":
            dhtDevice = validateDhtPin(hostName)

    
 
def detectDoorIn_Out(client, hostName):
    door_in = setdoorIn_Pin(hostName)
    door_out = setdoorOut_Pin(hostName)

    setupDoorIn_Out(door_in,door_out)

    GPIO.add_event_detect(door_in, GPIO.RISING, callback=lambda x: on_door_in(client))

    GPIO.add_event_detect(door_out, GPIO.RISING, callback=lambda x:on_door_out(client)) 

def on_door_in(client): 
    msg = "\nUma pessoa entrou na sala\n"
    client.send(f'\n{msg}\n'.encode('utf-8'))
    
def on_door_out(client): 
    msg = "\nUma pessoa saiu da sala\n"
    client.send(f'\n{msg}\n'.encode('utf-8'))


    

main()