
def print_input_state(sJanela,sPresenca,sFumaca,sPorta,sPessoasIn,sPessoasOut):
    return f" \
            Sensor da Janela:               {sJanela}\n \
            Sensor de Presença:             {sPresenca}\n \
            Sensor de Fumaça:               {sFumaca}\n \
            Sensor da porta:                {sPorta}\n \
            Sensor de entrada de pessoas:   {sPessoasIn}\n \
            Sensor de Saida de pessoas:     {sPessoasOut}\n"

def print_alert(inp):
    return f"\n !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n\
             \n !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n\
             \n !!!!!!!!!!!!!!  {inp} !!!!!!!!!!!!!!!!!! \n\
             \n !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n\
             \n !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n"  
    
def print_output_state(sLamp2, sLamp1, sAr, sProjetor, sAlarme):
    return f" \
            Lampada 2:          {sLamp2} \n \
            Lampada 1:          {sLamp1} \n \
            Ar-Condicionado:    {sAr} \n \
            Projetor:           {sProjetor} \n \
            Alarme:             {sAlarme}"


