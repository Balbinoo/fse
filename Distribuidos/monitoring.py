import RPi.GPIO as GPIO
from control import *
from view import *
from setup import check_AlarmState

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

def listenOutputs(hostName):
    lamp2, lamp1, ar, projetor, alarme = validateOutputPins(hostName)

    setupOutputs(lamp2, lamp1, ar, projetor, alarme)
    
    sLamp2, sLamp1, sAr, sProjetor, sAlarm = stateOutputs(lamp2, lamp1, ar, projetor, alarme)

    return print_output_state(sLamp2,sLamp1, sAr, sProjetor, sAlarm)

def listenInputs(hostName):
    janela,presenca,fumaca,porta,pessoasIn,pessoasOut = validateInputPins(hostName)
   
    setupInputs(janela,presenca,fumaca,porta,pessoasIn,pessoasOut)
    sJanela, sPresenca, sFumaca, sPorta, sPessoasIn, sPessoasOut = stateInputs(janela,presenca,fumaca,porta,pessoasIn,pessoasOut)
    alarmFire = alarmFireSystem(sFumaca, hostName)
    alarmRoom = alarmSystem(sJanela, sPresenca, sPorta, hostName)

    msg_output = print_input_state(sJanela,sPresenca,sFumaca,sPorta,
    sPessoasIn,sPessoasOut)   

    return msg_output, alarmFire, alarmRoom


#################

def alarmSystem(sJanela, sPresenca, sPorta, hostName):
    if check_AlarmState(hostName) == 1:
        if sJanela == 1: 
            return print_alert("ALERTA: JANELA!")
        if sPresenca == 1:
            return print_alert("ALERTA: PRESENÇA!")            
        if sPorta ==1:
            return print_alert("ALERTA: PORTA!")  
        else:
            return "0"  
    else:
        startLamps_15s(hostName)
        return "0"
        
def alarmFireSystem(sFumaca, hostName):
    if(sFumaca == 1):
        fire_Alarm(hostName)
        return print_alert("ALERTA: FUMAÇA!") 
    else:
        return "0"      
